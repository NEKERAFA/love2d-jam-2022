// Based on box blur shader of moonshine
// Reference: https://github.com/vrld/moonshine

extern vec2 direction;

vec4 effect(vec4 color, Image texture, vec2 tc, vec2 _) {
    vec4 c = vec4(0.0);

    for (float i = -12.0; i <= 12.0; i += 1.0)
    {
        c += Texel(texture, tc + i * direction);
    }

    return c / (24.0 + 1.0) * color;
}
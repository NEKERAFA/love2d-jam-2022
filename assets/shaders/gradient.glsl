vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords)
{
    vec4 light = vec4(0.3 * (1.0 - texture_coords.y), 0.3 * (1.0 - texture_coords.y), 0.3 * (1.0 - texture_coords.y), 1.0);
    vec4 texturecolor = Texel(tex, texture_coords);
    return texturecolor * color + light;
}
_DEBUG = false

function love.conf(t)
    t.identity = "ShakingAsteroids"
    t.window.icon = "icon.png"
    t.window.title = "Shaking Asteroids"
    t.window.width = 1080
    t.window.height = 720
    t.window.display = 2
end


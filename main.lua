local gamestate = require "libs.hump.gamestate"

local disclaimer = require "src.scenes.disclaimer"
local game = require "src.scenes.game"
local inputs = require "src.managers.inputs"
local fonts = require "src.managers.fonts"
local config = require "src.managers.configuration"
local sounds = require "src.managers.sounds"

function love.load()
    config:load()
    sounds:init()
    fonts:init()

    gamestate.registerEvents()

    if _DEBUG then
        gamestate.switch(game)
    else
        gamestate.switch(disclaimer)
    end
end

function love.update()
    inputs:update()
end
local class = require "libs.hump.class"
local signal = require "libs.hump.signal"
local hc = require "libs.hc"

local entity = require "src.entities.entity"

local bullet = class {
    __includes = entity
}

function bullet:init()
    entity.init(self, "bullet")

    signal.emit("shot")

    self.velocity = 600
    self.collider = hc.circle(0, 0, 10)
    self.collider.parent = self
end

function bullet:draw(shake)
    love.graphics.circle("fill", self.position.x + shake.x, self.position.y + shake.y, 10)

    if _DEBUG then
        local r, g, b, a = love.graphics.getColor()
        love.graphics.setColor(1, 0, 0, 0.5)
        self.collider:draw("fill")
        love.graphics.setColor(r, g, b, a)
    end
end

return bullet
local class = require "libs.hump.class"
local vector = require "libs.hump.vector"
local hc = require "libs.hc"

local entity = class()

function entity:init(type)
    self.position = vector(0, 0)
    self.vvel = vector(0, 0)
    self.velocity = 0
    self.type = type
end

function entity:release()
    if self.collider then
        hc.remove(self.collider)
    end
end

function entity:update(dt)
    self.position = self.position + self.velocity * dt * self.vvel
    if self.collider then
        self.collider:moveTo(self.position.x, self.position.y)
    end
end

function entity:draw(shake)
    if self.sprite then
        local origin = self.position + shake
        love.graphics.draw(self.sprite, origin.x, origin.y, 0, 1, 1, self.sprite:getWidth() / 2, self.sprite:getHeight() / 2)
    end

    if _DEBUG and self.collider then
        local r, g, b, a = love.graphics.getColor()
        love.graphics.setColor(1, 0, 0, 0.5)
        self.collider:draw("fill")
        love.graphics.setColor(r, g, b, a)
    end
end

return entity
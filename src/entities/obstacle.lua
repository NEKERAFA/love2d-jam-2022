local class = require "libs.hump.class"
local vector = require "libs.hump.vector"
local hc = require "libs.hc"

local entity = require "src.entities.entity"
local space = require "src.managers.space"

local obstacle = class {
    __includes = entity
}

function obstacle:setPosition()
    local minWidth, minHeight = space.x, space.y
    local maxWidth, maxHeight = space.width - math.abs(space.x), space.height - math.abs(space.y)

    self.position.x = love.math.random(minWidth, maxWidth)
    self.position.y = love.math.random(minHeight, maxHeight)

    self.vvel.x = love.math.random() * 2 - 1
    self.vvel.y = love.math.random() * 2 - 1

    if self.origin == "top" then
        self.position.y = minHeight
        self.vvel.y = math.abs(self.vvel.y)
    elseif self.origin == "bottom" then
        self.position.y = maxHeight
        self.vvel.y = - math.abs(self.vvel.y)
    elseif self.origin == "left" then
        self.position.x = minWidth
        self.vvel.x = math.abs(self.vvel.x)
    elseif self.origin == "right" then
        self.position.x = maxWidth
        self.vvel.x = - math.abs(self.vvel.x)
    end

    self.vvel:normalizeInplace()
end

function obstacle:init(origin)
    entity.init(self, "obstacle")
    self.rotation = 0
    self.dtheta = (love.math.random() * 2 - 1)
    self.o = math.pi
    self.velocity = 256
    self.origin = origin

    local obstacle = love.math.random(1, 2)
    self.sprite = love.graphics.newImage(string.format("assets/sprites/obstacle%i.png", obstacle))
    self.scale = 0.75

    self.collider = hc.circle(0, 0, 40)
    self.collider.parent = self

    self:setPosition()
    self.collider:moveTo(self.position.x, self.position.y)
end

function obstacle:update(dt)
    entity.update(self, dt)
    self.rotation = self.rotation + self.o * self.dtheta * dt
end

function obstacle:draw(shake)
    if not shake then
        shake = vector(0, 0)
    end

    local origin = self.position + shake
    love.graphics.draw(self.sprite, origin.x, origin.y, self.rotation, 0.75, 0.75, self.sprite:getWidth() / 2, self.sprite:getHeight() / 2)

    if _DEBUG then
        local r, g, b, a = love.graphics.getColor()
        love.graphics.setColor(1, 0, 0, 0.5)
        self.collider:draw("fill")
        love.graphics.setColor(r, g, b, a)
    end
end

return obstacle
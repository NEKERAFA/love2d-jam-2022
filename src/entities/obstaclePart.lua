local class = require "libs.hump.class"
local entity = require "src.entities.entity"

local obstaclePart = class {
    __includes = entity,
    maxPart = 8,
    sprite = love.graphics.newImage("assets/sprites/obstacle-pieces.png")
}

local parts = {
    love.graphics.newQuad(0, 0, 62, 54, obstaclePart.sprite),
    love.graphics.newQuad(62, 0, 38, 60, obstaclePart.sprite),
    love.graphics.newQuad(100, 0, 63, 51, obstaclePart.sprite),
    love.graphics.newQuad(0, 60, 64, 52, obstaclePart.sprite),
    love.graphics.newQuad(54, 60, 49, 41, obstaclePart.sprite),
    love.graphics.newQuad(103, 60, 88, 36, obstaclePart.sprite),
    love.graphics.newQuad(0, 112, 55, 70, obstaclePart.sprite),
    love.graphics.newQuad(55, 112, 63, 51, obstaclePart.sprite)
}

function obstaclePart:init(x, y, part)
    entity.init(self, "obstaclePart")
    self.quad = parts[part]

    self.position.x = x - 48
    self.position.y = y - 48

    if part == 1 then
        self.position.x = x + 80 * 0.75
        self.position.y = y + 33 * 0.75
    elseif part == 2 then
        self.position.x = x + 69 * 0.75
        self.position.y = y + 77 * 0.75
    elseif part == 3 then
        self.position.x = x + 33 * 0.75
        self.position.y = y + 58 * 0.75
    elseif part == 4 then
        self.position.x = x + 52 * 0.75
        self.position.y = y + 41 * 0.75
    elseif part == 5 then
        self.position.x = x + 31 * 0.75
        self.position.y = y + 98 * 0.75
    elseif part == 6 then
        self.position.x = x + 72 * 0.75
        self.position.y = y + 110 * 0.75
    elseif part == 7 then
        self.position.x = x + 100 * 0.75
        self.position.y = y + 74 * 0.75
    elseif part == 8 then
        self.position.x = x + 32 * 0.75
        self.position.y = y + 26 * 0.75
    end

    self.life = 0.5
    self.rotation = 0
    self.dtheta = (love.math.random() * 2 - 1)
    self.o = math.pi
    self.velocity = 512
    self.vvel.x = love.math.random() * 2 - 1
    self.vvel.y = love.math.random() * 2 - 1
end

function obstaclePart:update(dt)
    entity.update(self, dt)
    self.life = math.max(0, self.life - dt)
    self.rotation = self.rotation + self.o * self.dtheta * dt
end

function obstaclePart:draw(shake)
    local origin = self.position + shake
    local viewport = { self.quad:getViewport() }
    local r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(1, 1, 1, self.life / 0.5)
    love.graphics.draw(self.sprite, self.quad, origin.x, origin.y, self.rotation, 0.75, 0.75, viewport[3] / 2, viewport[4] / 2)
    love.graphics.setColor(r, g, b, a)
end

return obstaclePart
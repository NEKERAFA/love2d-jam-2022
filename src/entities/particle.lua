local class = require "libs.hump.class"

local entity = require "src.entities.entity"
local space = require "src.managers.space"

local particle = class {
    __includes = entity
}

function particle:init()
    entity.init(self, "particle")
    self.velocity = 16
    self.radius = love.math.random() * 4 + 4
    self.vvel.x = love.math.random() * 2 - 1
    self.vvel.y = love.math.random() * 2 - 1
    self.vvel:normalizeInplace()

    self.timer = love.math.random()
end

function particle:update(dt)
    entity.update(self, dt)

    self.timer = self.timer - dt
    if self.timer <= 0 then
        self.vvel.x = love.math.random() * 2 - 1
        self.vvel.y = love.math.random() * 2 - 1
        self.vvel:normalizeInplace()

        self.timer = love.math.random()
    end
end

function particle:draw(shake)
    local origin = self.position
    if shake then
        origin = self.position + shake
    end

    local r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(1, 1, 1, 0.15)
    love.graphics.circle("fill", origin.x, origin.y, self.radius)
    love.graphics.setColor(1, 1, 1, 0.05)
    love.graphics.circle("line", origin.x, origin.y, self.radius)
    love.graphics.setColor(r, g, b, a)
end

return particle
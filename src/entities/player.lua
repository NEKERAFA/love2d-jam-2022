local class = require "libs.hump.class"
local vector = require "libs.hump.vector"
local signal = require "libs.hump.signal"
local tween = require "libs.tween.tween"
local hc = require "libs.hc"

local entity = require "src.entities.entity"
local bullet = require "src.entities.bullet"
local inputs = require "src.managers.inputs"
local entities = require "src.managers.entities"

local width, height = love.graphics.getDimensions()
local origin = vector(width / 2, height / 2)
local inertia = 0.2
local max_player_move = 64

local player = class {
    __includes = entity
}

function player:init()
    entity.init(self, "player")

    self.position.x = origin.x
    self.position.y = origin.y
    self.collider = hc.polygon(54, 0, 108, 108, 0, 108)
    self.collider.parent = self
    self.collider:moveTo(origin.x, origin.y)
    self.rotation = 0
    self.sprite = love.graphics.newImage("assets/sprites/player.png")

    self.up = tween.new(inertia, self.position, {y = origin.y - max_player_move}, tween.easing.quad)
    self.up.state = "paused"
    self.down = tween.new(inertia, self.position, {y = origin.y + max_player_move}, tween.easing.quad)
    self.down.state = "paused"
    self.left = tween.new(inertia, self.position, {x = origin.x - max_player_move}, tween.easing.quad)
    self.left.state = "paused"
    self.right = tween.new(inertia, self.position, {x = origin.x + max_player_move}, tween.easing.quad)
    self.right.state = "paused"

    self.enabled = true
    self.gameover = signal.register("gameover", function()
        self.enabled = false
    end)

    self.shotTime = 0.2
    self.firstClickReleased = false
    self.mousePosition = { love.mouse.getPosition() }
end

function player:release()
    self.sprite:release()
    signal.remove("gameover", self.gameover)
end

function player:checkTweens()
    if inputs:down("up") and self.enabled then
        if self.down.state == "increasing" then
            self.down.state = "decreasing"
        elseif self.down.state == "paused" then
            self.up.state = "increasing"
        end
    else
        if self.up.state == "increasing" then
            self.up.state = "decreasing"
        end
    end

    if inputs:down("down") and self.enabled then
        if self.up.state == "increasing" then
            self.up.state = "decreasing"
        elseif self.up.state == "paused" then
            self.down.state = "increasing"
        end
    else
        if self.down.state == "increasing" then
            self.down.state = "decreasing"
        end
    end

    if inputs:down("left") and self.enabled then
        if self.right.state == "increasing" then
            self.right.state = "decreasing"
        elseif self.right.state == "paused" then
            self.left.state = "increasing"
        end
    else
        if self.left.state == "increasing" then
            self.left.state = "decreasing"
        end
    end

    if inputs:down("right") and self.enabled then
        if self.left.state == "increasing" then
            self.left.state = "decreasing"
        elseif self.left.state == "paused" then
            self.right.state = "increasing"
        end
    else
        if self.right.state == "increasing" then
            self.right.state = "decreasing"
        end
    end
end

function player:updateTweens(dt)
    if self.up.state == "increasing" then
        self.up:update(dt)
    elseif self.up.state == "decreasing" then
        self.up:update(-dt)

        if self.position.y == origin.y then
            self.up.state = "paused"
        end
    end

    if self.down.state == "increasing" then
        self.down:update(dt)
    elseif self.down.state == "decreasing" then
        self.down:update(-dt)

        if self.position.y == origin.y then
            self.down.state = "paused"
        end
    end

    if self.left.state == "increasing" then
        self.left:update(dt)
    elseif self.left.state == "decreasing" then
        self.left:update(-dt)

        if self.position.x == origin.x then
            self.left.state = "paused"
        end
    end

    if self.right.state == "increasing" then
        self.right:update(dt)
    elseif self.right.state == "decreasing" then
        self.right:update(-dt)

        if self.position.x == origin.x then
            self.right.state = "paused"
        end
    end
end

local space = love.physics.newRectangleShape(0, 0, width, height)
local up = vector(0, -1)
local mouse = vector(0, -1)

function player:checkShot(dt)
    if inputs:down("fire") and self.firstClickReleased and self.enabled then
        if self.shotTime == 0.2 then
            local obj = bullet()

            local delta = mouse:normalized()
            obj.position.x = self.position.x + delta.x * 64
            obj.position.y = self.position.y + delta.y * 64
            obj.collider:moveTo(obj.position.x, obj.position.y)

            obj.vvel.x = delta.x
            obj.vvel.y = delta.y

            entities:add(obj)
        end

        self.shotTime = self.shotTime - dt

        if self.shotTime <= 0 then
            self.shotTime = 0.2
        end
    elseif self.shotTime < 0.2 then
        self.shotTime = 0.2
    end

    if inputs:released("fire") then
        self.firstClickReleased = true
    end
end

function player:checkRotation(dt)
    if inputs:down("rotate_left") then
        local delta = -4 * dt
        self.rotation = self.rotation + delta
        mouse:rotateInplace(delta)
    elseif inputs:down("rotate_right") then
        local delta = 4 * dt
        self.rotation = self.rotation + delta
        mouse:rotateInplace(delta)
    end

    local mousePosition = { love.mouse.getPosition() }

    if self.mousePosition[1] ~= mousePosition[1] or self.mousePosition[2] ~= mousePosition[2] then
        if space:testPoint(self.position.x, self.position.y, 0, mousePosition[1], mousePosition[2]) and self.enabled then
            mouse.x = mousePosition[1] - width / 2
            mouse.y = mousePosition[2] - height / 2

            self.rotation = mouse:angleTo(up)
            self.mousePosition = mousePosition
        end
    end
end

function player:update(dt)
    self:checkTweens()
    self:updateTweens(dt)

    self:checkRotation(dt)
    self:checkShot(dt)
    self.collider:setRotation(self.rotation)

    local delta = mouse:normalized() * -20
    self.collider:moveTo(self.position.x + delta.x, self.position.y + delta.y)
end

function player:draw(shake)
    if self.sprite then
        local position = self.position + shake
        love.graphics.draw(self.sprite, position.x, position.y, self.rotation, 1, 1, self.sprite:getWidth() / 2, self.sprite:getHeight() / 2)
    end

    if _DEBUG and self.collider then
        local r, g, b, a = love.graphics.getColor()
        love.graphics.setColor(0, 1, 0)
        love.graphics.line(width / 2, height / 2, width / 2 + up.x, height / 2 + up.y * 50)
        love.graphics.line(width / 2, height / 2, width / 2 + mouse.x, height / 2 + mouse.y)

        love.graphics.setColor(1, 0, 0, 0.5)
        self.collider:draw("fill")
        love.graphics.setColor(r, g, b, a)
    end
end

return player
local entities = require "src.managers.entities"

local bullets = {}

local width, height = love.graphics.getDimensions()
local spaceWidth, spaceHeight = width + 32, height + 32
local space = love.physics.newRectangleShape(- 16, - 16, width + 32, height + 32)

function bullets:update()
    local bulletsToRemove = {}

    for entity in entities:iterate() do
        if entity.type == "bullet" then
            if not space:testPoint(spaceWidth / 2, spaceHeight / 2, 0, entity.position.x, entity.position.y) then
                table.insert(bulletsToRemove, entity)
            end
        end
    end

    for _, bullet in ipairs(bulletsToRemove) do
        entities:remove(bullet)
    end
end

return bullets
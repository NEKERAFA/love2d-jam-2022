local configuration = {}

local conf_path = love.filesystem.getSaveDirectory() .. "/config"

function configuration:load()
    local data = {
        volume_master = 0.5,
        volume_music = 1,
        volume_fx = 1,
        shake = 1,
        dyslexic = false
    }

    if love.filesystem.getInfo("config") then
        local conf_file = assert(io.open(conf_path, "r"))
        local buff = "return {\n"

        local first = true
        for line in conf_file:lines() do
            if first then
                first = false
            else
                buff = buff .. ",\n"
            end

            buff = buff .. line
        end

        conf_file:close()

        buff = buff .. "\n}"

        if _DEBUG then
            print(buff)
        end

        data = assert(loadstring(buff), conf_path)()
    end

    for key, value in pairs(data) do
        if tonumber(value) then
            self[key] = tonumber(value)
        elseif value == "true" then
            self[key] = true
        elseif value == "false" then
            self[key] = false
        else
            self[key] = value
        end
    end
end

function configuration:save()
    local conf_file = assert(love.filesystem.newFile("config", "w"))

    for key, value in pairs(self) do
        if key ~= "load" and key ~= "save" then
            conf_file:write(key .. " = " .. tostring(value) .. "\n")
        end
    end

    conf_file:flush()
    conf_file:close()
end

return configuration
local entities = {
    pool = {}
}

function entities:add(entity)
    table.insert(self.pool, entity)
end

function entities:remove(entity)
    local pos = 0
    for i, e in ipairs(self.pool) do
        if e == entity then
            pos = i
        end
    end

    if pos > 0 then
        table.remove(self.pool, pos)
        entity:release()
    end
end

function entities:iterate()
    local pos = 0

    return function()
        pos = pos + 1
        return self.pool[pos]
    end
end

function entities:clear()
    for entity in entities:iterate() do
        entity:release()
    end

    self.pool = {}
end

function entities:update(dt)
    for entity in self:iterate() do
        entity:update(dt)
    end
end

function entities:draw(shake)
    for entity in self:iterate() do
        entity:draw(shake)
    end
end

return entities
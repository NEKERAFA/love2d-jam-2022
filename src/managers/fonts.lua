local config = require "src.managers.configuration"

local font = {}

local normal = "Jellee-Bold"
local dyslexic = "OpenDyslexic-Regular"

function font:init()
    self:load()

    self.disclaimer = love.graphics.newFont(love.graphics.getFont():getHeight() * 1.25)
end

function font:load()
    local font_file = "assets/fonts/" .. normal .. ".ttf"
    if config.dyslexic then
        font_file = "assets/fonts/" .. dyslexic .. ".ttf"
    end

    self.normal = love.graphics.newFont(font_file, 32)
    self.title = love.graphics.newFont(font_file, 48)
end

function font:reset()
    self.normal:release()
    self.title:release()

    self:load()
end

function font:getFont()
    return self.normal
end

function font:getTitleFont()
    return self.title
end

function font:getDisclaimerFont()
    return self.disclaimer
end

return font
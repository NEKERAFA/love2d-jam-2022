local baton = require "libs.baton.baton"

local inputs = baton.new {
    controls = {
        left = {"key:a"},
        right = {"key:d"},
        up = {"key:w"},
        down = {"key:s"},
        rotate_left = {"key:q"},
        rotate_right = {"key:e"},
        fire = {"key:space", "mouse:1"},
        ui_up = {"key:up"},
        ui_down = {"key:down"},
        ui_left = {"key:left"},
        ui_right = {"key:right"},
        ui_confirm = {"key:return", "mouse:1"},
        ui_cancel = {"key:escape", "key:backspace"}
    },
}

return inputs
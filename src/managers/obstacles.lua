local signal = require "libs.hump.signal"

local obstacle = require "src.entities.obstacle"
local obstaclePart = require "src.entities.obstaclePart"
local entities = require "src.managers.entities"
local space = require "src.managers.space"

local obstacles = {}

local origins = {"top", "bottom", "left", "right"}

function obstacles:init()
    space:init()

    for _, origin in ipairs(origins) do
        for _ = 1, 2 do
            entities:add(obstacle(origin))
        end
    end
end

function obstacles:reset(obtacle)
    local minWidth, minHeight = space.x, space.y
    local maxWidth, maxHeight = space.width - math.abs(space.x), space.height - math.abs(space.y)

    obtacle.radius = love.math.random() * 5 + 5
    obtacle.vvel.x = love.math.random() * 2 - 1
    obtacle.vvel.y = love.math.random() * 2 - 1

    local obtacle_x, obtacle_y = obtacle.position.x, obtacle.position.y
    obtacle.position.x = love.math.random(minWidth, maxWidth)
    obtacle.position.y = love.math.random(minHeight, maxHeight)

    if obtacle_x <= minWidth then
        obtacle.position.x = maxWidth
        obtacle.vvel.x = - math.abs(obtacle.vvel.x)
    elseif obtacle_x >= maxWidth then
        obtacle.position.x = minWidth
        obtacle.vvel.x = math.abs(obtacle.vvel.x)
    end

    if obtacle_y <= space.y then
        obtacle.position.y = maxHeight
        obtacle.vvel.y = - math.abs(obtacle.vvel.y)
    elseif obtacle_y >= maxHeight then
        obtacle.position.y = minHeight
        obtacle.vvel.y = math.abs(obtacle.vvel.y)
    end

    obtacle.vvel:normalizeInplace()
end

function obstacles:add()
    local origin = love.math.random(4)
    entities:add(obstacle(origins[origin]))
end

function obstacles:remove(entity)
    signal.emit("explosion")

    for i = 1, obstaclePart.maxPart do
        entities:add(obstaclePart(entity.position.x, entity.position.y, i))
    end

    entities:remove(entity)
    self:add()
end

function obstacles:update()
    local entitiesToRemove = {}

    for entity in entities:iterate() do
        if entity.type == "obstacle" then
            if not space.shape:testPoint(space.width / 2, space.height / 2, 0, entity.position.x, entity.position.y) then
                self:reset(entity)
            end
        elseif entity.type == "obstaclePart" and entity.life == 0 then
            table.insert(entitiesToRemove, entity)
        end
    end

    for _, entity in ipairs(entitiesToRemove) do
        entities:remove(entity)
    end
end

return obstacles
local entities = require "src.managers.entities"
local space = require "src.managers.space"
local particle = require "src.entities.particle"

local particles = {}

function particles:init(n)
    for _ = 1, tonumber(n) do
        local entity = particle()
        entity.position.x = love.math.random(space.x, space.width - math.abs(space.x))
        entity.position.y = love.math.random(space.y, space.height - math.abs(space.y))
        entities:add(entity)
    end
end

function particles:reset(entity)
    local width, height = love.graphics.getDimensions()
    local minWidth, minHeight = space.x, space.y
    local maxWidth, maxHeight = space.width - math.abs(space.x), space.height - math.abs(space.y)

    entity.radius = love.math.random() * 5 + 5
    entity.vvel.x = love.math.random() * 2 - 1
    entity.vvel.y = love.math.random() * 2 - 1

    local entity_x, entity_y = entity.position.x, entity.position.y
    entity.position.x = love.math.random(space.x, space.width)
    entity.position.y = love.math.random(space.y, space.height)

    if entity_x <= minWidth then
        entity.position.x = love.math.random(width, maxWidth)
        entity.vvel.x = - math.abs(entity.vvel.x)
    elseif entity_x >= maxWidth then
        entity.position.x = love.math.random(minWidth, 0)
        entity.vvel.x = math.abs(entity.vvel.x)
    end

    if entity_y <= space.y then
        entity.position.y = love.math.random(height, maxHeight)
        entity.vvel.y = - math.abs(entity.vvel.y)
    elseif entity_y >= maxHeight then
        entity.position.y = love.math.random(minHeight, 0)
        entity.vvel.y = math.abs(entity.vvel.y)
    end

    entity.vvel:normalizeInplace()
end

function particles:update()
    for entity in entities:iterate() do
        if entity.type == "particle" then
            if not space.shape:testPoint(space.width / 2, space.height / 2, 0, entity.position.x, entity.position.y)then
                self:reset(entity)
            end
        end
    end
end

return particles
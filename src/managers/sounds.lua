local signal = require "libs.hump.signal"
local timer = require "libs.hump.timer"

local config = require "src.managers.configuration"

local sounds = {
    sfx = {
        explosion = love.audio.newSource("assets/sfx/explosion.wav", "static"),
        select = love.audio.newSource("assets/sfx/select.wav", "static"),
        shot = love.audio.newSource("assets/sfx/shot.wav", "static")
    },
    ost = {
        game = love.audio.newSource("assets/ost/game.wav", "static"),
        menu = love.audio.newSource("assets/ost/menu.wav", "static"),
        time_running_out = love.audio.newSource("assets/ost/time_running_out.wav", "static")
    }
}

function sounds:init()
    self:setVolumes()

    for _, sound in pairs(self.ost) do
        sound:setLooping(true)
    end

    signal.register("explosion", function()
        self.sfx.explosion:play()
    end)

    signal.register("gameover", function()
        self.sfx.explosion:play()
        self.ost.game:stop()
        self.ost.time_running_out:stop()
        timer.cancel(self.timer)
    end)

    signal.register("shot", function()
        self.sfx.shot:play()
    end)
end

function sounds:startGame()
    self.ost.menu:pause()
    self:resume()
end

function sounds:resume()
    self.ost.game:play()
    self.ost.time_running_out:setVolume(0)
    self.ost.time_running_out:play()

    self.time = 0
    self.timer = timer.during(4 * 60, function (dt)
        self.time = self.time + dt
        self.ost.game:setVolume(config.volume_music - 1 / (4 * 60) * self.time * config.volume_music)
        self.ost.time_running_out:setVolume(1 / (4 * 60) * self.time * config.volume_music)
    end)
end

function sounds:setVolumes()
    love.audio.setVolume(config.volume_master)

    for _, sound in pairs(self.ost) do
        sound:setVolume(config.volume_music)
    end

    for _, sound in pairs(self.sfx) do
        sound:setVolume(config.volume_fx)
    end
end

return sounds
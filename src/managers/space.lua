local space = { x = -512, y = -512 }

function space:init()
    self.x = -512
    self.y = -512

    local width, height = love.graphics.getDimensions()
    self:setSpace(width, height)
end

function space:setSpace(width, height)
    if self.shape then
        self.shape:release()
    end

    self.width = width + 1024
    self.height = height + 1024
    self.shape = love.physics.newRectangleShape(self.x, self.y, self.width, self.height)
end

return space
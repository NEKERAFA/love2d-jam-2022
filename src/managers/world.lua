local vector = require "libs.hump.vector"
local signal = require "libs.hump.signal"
local tween = require "libs.tween.tween"

local entities = require "src.managers.entities"
local inputs = require "src.managers.inputs"
local config = require "src.managers.configuration"

local world = {}

local inertia = 0.5
local max_shake = 4 * 60
local max_move = 8

function world:init()
    self.move = vector(0, 0)
    self.shake = vector(0, 0)
    self.magnitude = 0

    self.up = tween.new(inertia, self.move, {y = 1}, tween.easing.quad)
    self.up.state = "paused"
    self.down = tween.new(inertia, self.move, {y = -1}, tween.easing.quad)
    self.down.state = "paused"
    self.left = tween.new(inertia, self.move, {x = 1}, tween.easing.quad)
    self.left.state = "paused"
    self.right = tween.new(inertia, self.move, {x = -1}, tween.easing.quad)
    self.right.state = "paused"

    self.enabled = true
    self.gameover = signal.register("gameover", function()
        self.enabled = false
    end)

    if not _DEBUG then
        self.shakeTween = tween.new(max_shake, self, {magnitude = 64 * config.shake}, tween.easing.inCubic)
    end
end

function world:clear()
    signal.remove("gameover", self.gameover)
end

function world:checkTweens()
    if inputs:down("up") and self.enabled then
        if self.down.state == "increasing" then
            self.down.state = "decreasing"
        elseif self.down.state == "paused" then
            self.up.state = "increasing"
        end
    else
        if self.up.state == "increasing" then
            self.up.state = "decreasing"
        end
    end

    if inputs:down("down") and self.enabled then
        if self.up.state == "increasing" then
            self.up.state = "decreasing"
        elseif self.up.state == "paused" then
            self.down.state = "increasing"
        end
    else
        if self.down.state == "increasing" then
            self.down.state = "decreasing"
        end
    end

    if inputs:down("left") and self.enabled then
        if self.right.state == "increasing" then
            self.right.state = "decreasing"
        elseif self.right.state == "paused" then
            self.left.state = "increasing"
        end
    else
        if self.left.state == "increasing" then
            self.left.state = "decreasing"
        end
    end

    if inputs:down("right") and self.enabled then
        if self.left.state == "increasing" then
            self.left.state = "decreasing"
        elseif self.left.state == "paused" then
            self.right.state = "increasing"
        end
    else
        if self.right.state == "increasing" then
            self.right.state = "decreasing"
        end
    end
end

function world:updateTweens(dt)
    if self.up.state == "increasing" then
        self.up:update(dt)
    elseif self.up.state == "decreasing" then
        self.up:update(-dt)

        if self.move.y == 0 then
            self.up.state = "paused"
        end
    end

    if self.down.state == "increasing" then
        self.down:update(dt)
    elseif self.down.state == "decreasing" then
        self.down:update(-dt)

        if self.move.y == 0 then
            self.down.state = "paused"
        end
    end

    if self.left.state == "increasing" then
        self.left:update(dt)
    elseif self.left.state == "decreasing" then
        self.left:update(-dt)

        if self.move.x == 0 then
            self.left.state = "paused"
        end
    end

    if self.right.state == "increasing" then
        self.right:update(dt)
    elseif self.right.state == "decreasing" then
        self.right:update(-dt)

        if self.move.x == 0 then
            self.right.state = "paused"
        end
    end
end

function world:update(dt)
    self:checkTweens()
    self:updateTweens(dt)
    if not _DEBUG then
        self.shakeTween:update(dt)
    end

    for entity in entities:iterate() do
        if entity.type ~= "player" and entity.type ~= "bullet" then
            entity.position = entity.position + max_move * self.move
        end

        self.shake.x = love.math.random() * 2 - 1
        self.shake.y = love.math.random() * 2 - 1
        self.shake:normalizeInplace()
    end

    entities:update(dt)
end

function world:draw()
    entities:draw(self.magnitude * self.shake)
end

return world
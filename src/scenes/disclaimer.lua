local gamestate = require "libs.hump.gamestate"
local splash = require "src.scenes.splash"
local fonts = require "src.managers.fonts"

local disclaimer = {clock = 0}

function disclaimer:update(dt)
    self.clock = math.min(5, self.clock + dt)

    if self.clock == 5 then
        gamestate.switch(splash)
    end
end

function disclaimer:draw()
    local width = love.graphics.getDimensions()
    local font = fonts:getDisclaimerFont()

    local widthTitle = font:getWidth("Warning: Photosensitivy")
    love.graphics.printf("Warning: Photosensitivy", font, width / 2 - widthTitle / 2, 275, widthTitle, "center")
    love.graphics.printf("A very small percentage of individuals may experience epileptic seizures or blackouts when exposed to certain light patterns or flashing lights. Exposure to certain patterns or backgrounds on a television screen or when playing video games may trigger epileptic seizures or blackouts in these individuals.  If you, or anyone in your family has an epileptic condition or has had seizures of any kind, consult your physician before playing.", 200, 300, width - 400, "justify")
end

return disclaimer
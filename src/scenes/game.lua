local signal = require "libs.hump.signal"
local timer = require "libs.hump.timer"
local gamestate = require "libs.hump.gamestate"
local hc = require "libs.hc"

local entities = require "src.managers.entities"
local space = require "src.managers.space"
local world = require "src.managers.world"
local bullets = require "src.managers.bullets"
local obstacles = require "src.managers.obstacles"
local particles = require "src.managers.particles"
local sounds = require "src.managers.sounds"
local inputs = require "src.managers.inputs"
local fonts = require "src.managers.fonts"

local player = require "src.entities.player"
local obstaclePart = require "src.entities.obstaclePart"
local mathUtils = require "src.utils.math"
local gradient = require "src.utils.gradient"

local gameoverLabel = require "src.ui.gameover"
local pointsLabel = require "src.ui.points"
local selectableLabel = require "src.ui.selectableLabel"

local game = { ui = {} }

local width, height = love.graphics.getDimensions()

function game:init()
    self.ui.gameover = gameoverLabel()
    self.ui.points = pointsLabel()
    self.ui.retry = selectableLabel("Retry", fonts:getFont(), width / 2, height / 2 + 100)
end

function game:load()
    self.player = player()
    self.background = { 0.353, 0.5, 1 }
    self.gameover = false
    self.collidedTime = 0.1

    self.points = 0
    self.ui.points:update(self.points)

    self.pointTimer = timer.every(1, function ()
        self.points = self.points + 1
        self.ui.points:update(self.points)
    end)

    if _DEBUG then
        space:init()
    end

    particles:init(1024)
    obstacles:init()
    entities:add(self.player)
    world:init()

    -- blue - 0
    self.tween = timer.tween(60, self.background, {0.196, 0.621, 0.166}, "in-quad", function ()
        -- green - 1
        timer.cancel(self.tween)
        self.tween = timer.tween(60, self.background, {1, 0.484, 0.135}, "in-quad", function ()
            -- orange - 2
            timer.cancel(self.tween)
            self.tween = timer.tween(60, self.background, {0.473, 0.127, 0.966}, "in-quad", function ()
                -- purple - 3
                timer.cancel(self.tween)
                self.tween = timer.tween(60, self.background, {1, 0.235, 0.913}, "in-quad") -- pink - 4
            end)
        end)
    end)
end

function game:enter(prev)
    entities:clear()

    if prev == self then
        self:load()
        sounds:resume()
    else
        self:load()
        sounds:startGame()
        self.mousePosition = { love.mouse.getPosition() }
    end
end

function game:checkPlayerCollision(dt)
    local collisions = hc.collisions(self.player.collider)
    local collided = false
    for shape in pairs(collisions) do
        if shape.parent.type == "obstacle" then
            collided = true
            break
        end
    end

    if collided then
        self.collidedTime = self.collidedTime - dt

        if self.collidedTime <= 0 and not _DEBUG then
            signal.emit("gameover")
            entities:remove(self.player)
            for i = 1, obstaclePart.maxPart do
                entities:add(obstaclePart(self.player.position.x, self.player.position.y, i))
            end
            self.gameover = true
            timer.cancel(self.pointTimer)
            sounds.ost.game:stop()
            timer.cancel(self.tween)
            self.tween = timer.tween(2, self.background, {0.8, 0.05, 0}, "in-quad")
        end
    elseif self.collidedTime < 0.1 then
        self.collidedTime = 0.1
    end
end

function game:checkBulletCollisions()
    for entity in entities:iterate() do
        if entity.type == "bullet" then
            local collisions = hc.collisions(entity.collider)
            local obstacle = false
            for shape in pairs(collisions) do
                if shape.parent.type == "obstacle" then
                    obstacle = shape.parent
                    break
                end
            end

            if obstacle then
                entities:remove(entity)
                obstacles:remove(obstacle)
                break
            end
        end
    end
end

function game:checkObstacleCount()
    local maxObstacles = 8 + math.floor(self.points / 30) * 2
    local currentObstacles = 0

    for entity in entities:iterate() do
        if entity.type == "obstacle" then
            currentObstacles = currentObstacles + 1
        end
    end

    if currentObstacles < maxObstacles then
        for _ = currentObstacles, maxObstacles do
            obstacles:add()
        end
    end
end

function game:checkRetry()
    local item = self.ui.retry

    if inputs:pressed("ui_confirm") and item.hover then
        gamestate.switch(self)
    end

    if inputs:pressed("ui_up") or inputs:pressed("ui_down") then
        item.hover = true
    end

    local mousePosition = { love.mouse.getPosition() }
    if self.mousePosition[1] ~= mousePosition[1] or self.mousePosition[2] ~= mousePosition[2] then
        item.hover = mathUtils.includeRect(item.position.x, item.position.y, item.width, item.height, mousePosition[1], mousePosition[2])
        self.mousePosition = mousePosition
    end
end

function game:update(dt)
    world:update(dt)

    -- Check particles
    particles:update()

    -- Check obstacles
    obstacles:update()
    if not self.gameover then
        self:checkPlayerCollision(dt)
    end

    -- Check bullets
    bullets:update()
    self:checkBulletCollisions()

    timer.update(dt)

    -- Check retry button
    if self.gameover then
        self:checkRetry()
    else
        self:checkObstacleCount()
    end
end

function game:drawUi()
    self.ui.points:draw()

    if self.gameover then
        self.ui.gameover:draw()
        self.ui.retry:draw()
    end
end

function game:draw()
    -- Draw background
    gradient:draw({ self.background[1], self.background[2], self.background[3] })

    -- Draw world
    world:draw()

    self:drawUi()

    if _DEBUG then
        love.graphics.print(string.format("entities: %i", #entities.pool), 10, 10)
    end
end

return game
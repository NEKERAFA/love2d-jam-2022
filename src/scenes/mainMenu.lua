local gamestate = require "libs.hump.gamestate"
local vector = require "libs.hump.vector"
local timer = require "libs.hump.timer"

local game = require "src.scenes.game"

local entities = require "src.managers.entities"
local space = require "src.managers.space"
local particles = require "src.managers.particles"
local obstacles = require "src.managers.obstacles"
local inputs = require "src.managers.inputs"
local sounds = require "src.managers.sounds"
local fonts = require "src.managers.fonts"
local config = require "src.managers.configuration"

local mathUtils = require "src.utils.math"
local gradient = require "src.utils.gradient"

local titleLabel = require "src.ui.title"
local label = require "src.ui.label"
local selectableLabel = require "src.ui.selectableLabel"

local mainMenu = { ui = {} }

local width, height = love.graphics.getDimensions()

function mainMenu:init()
    self:loadUi()
    self.state = "mainMenu"
    self.offset = vector(0, 0)
    self.tween = nil
end

function mainMenu:loadUi()
    self.ui.title = titleLabel()
    self.ui.startGame = selectableLabel("Start Game", fonts:getFont(), width / 2, height / 2 + 25)
    self.ui.options = selectableLabel("Options", fonts:getFont(), width / 2, height / 2 + 75)
    self.ui.copyright1 = label("Copyright (c) Rafael Alcalde Azpiazu 2022", fonts:getFont(), 10 + fonts:getFont():getWidth("Copyright (c) Rafael Alcalde Azpiazu 2022") / 2, height - 10 - fonts:getFont():getHeight() * 2)
    self.ui.copyright2 = label("Sounds by Jonatan Galindo Alvarez", fonts:getFont(), 10 + fonts:getFont():getWidth("Sounds by Jonatan Galindo Alvarez") / 2, height - 5 - fonts:getFont():getHeight())

    self.ui.master = label("Volume master", fonts:getFont(), width / 2 - 150, height + 100)
    self.ui.masterValue = selectableLabel(string.format("< %i%% >", config.volume_master / 0.5 * 100), fonts:getFont(), width / 2 + 100, height + 100)
    self.ui.music = label("Music", fonts:getFont(), width / 2 - 150, height + 140)
    self.ui.musicValue = selectableLabel(string.format("< %i%% >", config.volume_music * 100), fonts:getFont(), width / 2 + 100, height + 140)
    self.ui.sfx = label("Sound effects", fonts:getFont(), width / 2 - 150, height + 180)
    self.ui.sfxValue = selectableLabel(string.format("< %i%% >", config.volume_fx * 100), fonts:getFont(), width / 2 + 100, height + 180)
    self.ui.shake = label("Shake effect", fonts:getFont(), width / 2 - 150, height + 220)
    self.ui.shakeValue = selectableLabel(string.format("< %i%% >", config.shake * 100), fonts:getFont(), width / 2 + 100, height + 220)
    self.ui.dyslexic = label("Dyslexic Mode", fonts:getFont(), width / 2 - 150, height + 260)
    self.ui.dyslexicValue = selectableLabel(string.format("< %s >", tostring(config.dyslexic)), fonts:getFont(), width / 2 + 100, height + 260)
end

function mainMenu:enter()
    self.option = ""
    self.mousePosition = { love.mouse.getPosition() }

    space:init()
    particles:init(1024)
    obstacles:init()

    sounds.ost.menu:play()
end

function mainMenu:startGame()
    sounds.ost.menu:stop()
    gamestate.push(game)
end

function mainMenu:checkMainMenu()
    if inputs:pressed("ui_confirm") then
        if self.option == "startGame" then
            self:startGame()
        elseif self.option == "options" then
            self.state = "up"
            self.option = "masterValue"
            self.tween = timer.tween(1, self.offset, {y = -height}, "in-out-quad", function ()
                self.state = "options"
                timer.cancel(self.tween)
                self.tween = nil
            end)
        end
    end

    if inputs:pressed("ui_up") then
        self.option = "startGame"
    elseif inputs:pressed("ui_down") then
        if self.option == "" then
            self.option = "startGame"
        else
            self.option = "options"
        end
    end
end

function mainMenu:checkOptions()
    if inputs:pressed("ui_confirm") then
        if self.option == "dyslexicValue" then
            config.dyslexic = not config.dyslexic
            config:save()
            config:load()
            fonts:reset()
            mainMenu:loadUi()
        end
    end

    if inputs:pressed("ui_cancel") then
        self.state = "down"
        self.option = "options"
        self.tween = timer.tween(1, self.offset, {y = 0}, "in-out-quad", function ()
            self.state = "mainMenu"
            timer.cancel(self.tween)
            self.tween = nil
        end)
    end

    if inputs:pressed("ui_left") then
        if self.option == "masterValue" then
            config.volume_master = math.max(0, config.volume_master - 0.05)
            config:save()
            config:load()
            sounds:setVolumes()
            self.ui.masterValue:set(string.format("< %i%% >", config.volume_master / 0.5 * 100))
        elseif self.option == "musicValue" then
            config.volume_music = math.max(0, config.volume_music - 0.1)
            config:save()
            config:load()
            sounds:setVolumes()
            self.ui.musicValue:set(string.format("< %i%% >", config.volume_music * 100))
        elseif self.option == "sfxValue" then
            config.volume_fx = math.max(0, config.volume_fx - 0.1)
            config:save()
            config:load()
            sounds:setVolumes()
            self.ui.sfxValue:set(string.format("< %i%% >", config.volume_fx * 100))
        elseif self.option == "shakeValue" then
            config.shake = math.max(0.25, config.shake - 0.1)
            config:save()
            config:load()
            sounds:setVolumes()
            self.ui.shakeValue:set(string.format("< %i%% >", config.shake * 100))
        elseif self.option == "dyslexicValue" then
            config.dyslexic = not config.dyslexic
            config:save()
            config:load()
            fonts:reset()
            mainMenu:loadUi()
        end
    elseif inputs:pressed("ui_right") then
        if self.option == "masterValue" then
            config.volume_master = math.min(0.5, config.volume_master + 0.05)
            config:save()
            config:load()
            sounds:setVolumes()
            self.ui.masterValue:set(string.format("< %i%% >", config.volume_master / 0.5 * 100))
        elseif self.option == "musicValue" then
            config.volume_music = math.min(1, config.volume_music + 0.1)
            config:save()
            config:load()
            sounds:setVolumes()
            self.ui.musicValue:set(string.format("< %i%% >", config.volume_music * 100))
        elseif self.option == "sfxValue" then
            config.volume_fx = math.min(1, config.volume_fx + 0.1)
            config:save()
            config:load()
            sounds:setVolumes()
            self.ui.sfxValue:set(string.format("< %i%% >", config.volume_fx * 100))
        elseif self.option == "shakeValue" then
            config.shake = math.min(1, config.shake + 0.1)
            config:save()
            config:load()
            sounds:setVolumes()
            self.ui.shakeValue:set(string.format("< %i%% >", config.shake * 100))
        elseif self.option == "dyslexicValue" then
            config.dyslexic = not config.dyslexic
            self.ui.dyslexicValue:set(string.format("< %s >", tostring(config.dyslexic)))
            config:save()
            config:load()
            fonts:reset()
            mainMenu:loadUi()
        end
    end

    if inputs:pressed("ui_up") then
        if self.option == "" then
            self.option = "masterValue"
        elseif self.option == "dyslexicValue" then
            self.option = "shakeValue"
        elseif self.option == "shakeValue" then
            self.option = "sfxValue"
        elseif self.option == "sfxValue" then
            self.option = "musicValue"
        elseif self.option == "musicValue" then
            self.option = "masterValue"
        end
    elseif inputs:pressed("ui_down") then
        if self.option == "" then
            self.option = "masterValue"
        elseif self.option == "masterValue" then
            self.option = "musicValue"
        elseif self.option == "musicValue" then
            self.option = "sfxValue"
        elseif self.option == "sfxValue" then
            self.option = "shakeValue"
        elseif self.option == "shakeValue" then
            self.option = "dyslexicValue"
        end
    end
end

function mainMenu:checkMenu()
    if self.state == "mainMenu" then
        self:checkMainMenu()
    elseif self.state == "options" then
        self:checkOptions()
    end

    local mousePosition = { love.mouse.getPosition() }

    if self.mousePosition[1] ~= mousePosition[1] or self.mousePosition[2] ~= mousePosition[2] then
        local selected = ""
        for key, item in pairs(self.ui) do
            if item.hover ~= nil and
                mathUtils.includeRect(item.position.x - item.width / 2, item.position.y - item.height / 2 + self.offset.y, item.width, item.height, mousePosition[1], mousePosition[2]) then
                selected = key
            end
        end

        self.option = selected
        self.mousePosition = mousePosition
    end

    for key, item in pairs(self.ui) do
        if key ~= "title" then
            item.hover = key == self.option
        end
    end
end

function mainMenu:update(dt)
    entities:update(dt)
    particles:update()
    obstacles:update()

    self:checkMenu()
    if self.tween then
        timer.update(dt)
    end
end

function mainMenu:draw()
    gradient:draw({ 0.353, 0.5, 1 })
    entities:draw()

    for _, item in pairs(self.ui) do
        item:draw(self.offset)
    end
end

return mainMenu
local gamestate = require "libs.hump.gamestate"
local lib = require "libs.splashes.o-ten-one"

local mainMenu = require "src.scenes.mainMenu"

local splash = {}

function splash:enter()
    self.obj = lib.new({ background = {0, 0, 0} })
    self.obj.onDone = function ()
        love.graphics.setColor(1, 1, 1, 1)
        gamestate.switch(mainMenu)
    end
end

function splash:update(dt)
    self.obj:update(dt)
end

function splash:draw()
    self.obj:draw()
end

return splash
local class = require "libs.hump.class"

local fonts = require "src.managers.fonts"

local gameoverUi = class()

function gameoverUi:init()
    local font = fonts:getTitleFont()
    local width = font:getWidth("Game over")

    self.sprite = love.graphics.newCanvas(width + 2, font:getHeight() + 2)
    self.sprite:renderTo(function ()
        love.graphics.print({{0, 0, 0, 1}, "Game over"}, font, 0, 0)
        love.graphics.print({{0, 0, 0, 1}, "Game over"}, font, 2, 0)
        love.graphics.print({{0, 0, 0, 1}, "Game over"}, font, 0, 2)
        love.graphics.print({{0, 0, 0, 1}, "Game over"}, font, 2, 2)
        love.graphics.print({{1, 1, 1, 1}, "Game over"}, font, 1, 1)
    end)
end

local width, height = love.graphics.getDimensions()

function gameoverUi:draw()
    love.graphics.draw(self.sprite, width / 2, height / 2, 0, 1, 1, self.sprite:getWidth() / 2, self.sprite:getHeight() / 2)
end

return gameoverUi
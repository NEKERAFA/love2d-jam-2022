local class = require "libs.hump.class"
local vector = require "libs.hump.vector"

local label = class()

function label:init(text, font, x, y)
    self.sprite = love.graphics.newText(font, text)
    self.position = vector(x, y)
    self.width = self.sprite:getWidth()
    self.height = self.sprite:getHeight()
end

function label:set(text)
    self.sprite:set(tostring(text))
end

function label:draw(offset)
    if not offset then
        offset = vector(0, 0)
    end

    local r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(0, 0, 0, 1)
    love.graphics.draw(self.sprite, self.position.x - 1 + offset.x, self.position.y - 1 + offset.y, 0, 1, 1, self.sprite:getWidth() / 2, self.sprite:getHeight() / 2)
    love.graphics.draw(self.sprite, self.position.x + 1 + offset.x, self.position.y - 1 + offset.y, 0, 1, 1, self.sprite:getWidth() / 2, self.sprite:getHeight() / 2)
    love.graphics.draw(self.sprite, self.position.x - 1 + offset.x, self.position.y + 1 + offset.y, 0, 1, 1, self.sprite:getWidth() / 2, self.sprite:getHeight() / 2)
    love.graphics.draw(self.sprite, self.position.x + 1 + offset.x, self.position.y + 1 + offset.y, 0, 1, 1, self.sprite:getWidth() / 2, self.sprite:getHeight() / 2)
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.draw(self.sprite, self.position.x + offset.x, self.position.y + offset.y, 0, 1, 1, self.sprite:getWidth() / 2, self.sprite:getHeight() / 2)
    love.graphics.setColor(r, g, b, a)
end

return label
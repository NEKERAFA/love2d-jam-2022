local class = require "libs.hump.class"

local fonts = require "src.managers.fonts"

local pointsUi = class()

function pointsUi:init()
    self.sprite = love.graphics.newText(fonts:getFont(), "0")
end

function pointsUi:update(points)
    self.sprite:set(tostring(points))
end

function pointsUi:draw()
    local r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(0, 0, 0, 1)
    love.graphics.draw(self.sprite, love.graphics.getWidth() - 15, 15, 0, 1, 1, self.sprite:getWidth(), 0)
    love.graphics.draw(self.sprite, love.graphics.getWidth() - 17, 15, 0, 1, 1, self.sprite:getWidth(), 0)
    love.graphics.draw(self.sprite, love.graphics.getWidth() - 15, 17, 0, 1, 1, self.sprite:getWidth(), 0)
    love.graphics.draw(self.sprite, love.graphics.getWidth() - 17, 17, 0, 1, 1, self.sprite:getWidth(), 0)
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.draw(self.sprite, love.graphics.getWidth() - 16, 16, 0, 1, 1, self.sprite:getWidth(), 0)
    love.graphics.setColor(r, g, b, a)
end

return pointsUi
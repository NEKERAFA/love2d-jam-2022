local class = require "libs.hump.class"
local vector = require "libs.hump.vector"

local boxblur = require "src.utils.boxblur"

local label = require "src.ui.label"

local selectableLabel = class {
    __includes = label
}

local effect = love.graphics.newShader("assets/shaders/white.glsl")

function selectableLabel:init(text, font, x, y)
    label.init(self, text, font, x, y)

    local width = font:getWidth(text)
    self.halo = boxblur.effect(width, font:getHeight(), function ()
        love.graphics.setColor(1, 1, 1, 1)
        love.graphics.print(text, font, 12, 12)
    end)

    self.hover = false
end

function selectableLabel:draw(offset)
    if not offset then
        offset = vector(0, 0)
    end

    if self.hover then
        love.graphics.setShader(effect)
        love.graphics.draw(self.halo, self.position.x + offset.x, self.position.y + offset.y, 0, 1, 1, self.halo:getWidth() / 2, self.halo:getHeight() / 2)
        love.graphics.setShader()
    end

    label.draw(self, offset)
end

return selectableLabel
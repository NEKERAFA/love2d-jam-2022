local class = require "libs.hump.class"

local fonts = require "src.managers.fonts"

local titleLabel = class()

function titleLabel:init()
    local font = fonts:getTitleFont()
    local width1 = font:getWidth("Shaking")
    local width2 = font:getWidth("Asteroids")
    local width = (width1 > width2 and width1) or width2

    self.sprite = love.graphics.newCanvas(width + 2, font:getHeight() * 2 + 2)
    self.sprite:renderTo(function ()
        love.graphics.setColor(0, 0, 0, 1)
        love.graphics.printf("Shaking\nAsteroids", font, 0, 0, width, "center")
        love.graphics.printf("Shaking\nAsteroids", font, 2, 0, width, "center")
        love.graphics.printf("Shaking\nAsteroids", font, 0, 2, width, "center")
        love.graphics.printf("Shaking\nAsteroids", font, 2, 2, width, "center")
        love.graphics.setColor(1, 1, 1, 1)
        love.graphics.printf("Shaking\nAsteroids", font, 1, 1, width, "center")
    end)
end

function titleLabel:draw(offset)
    local width, height = love.graphics.getDimensions()
    love.graphics.draw(self.sprite, width / 2, height / 2 - 150 + offset.y, 0, 1, 1, self.sprite:getWidth() / 2, self.sprite:getHeight() / 2)
end

return titleLabel
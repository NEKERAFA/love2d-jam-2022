local boxblur = {}

local _material_shadow_shader = love.graphics.newShader("assets/shaders/boxblur.glsl")

function boxblur.effect(width, height, onDraw)
    local _love_canvas = love.graphics.getCanvas()
    local _love_shader = love.graphics.getShader()
    local _love_blendmode, _love_alphamode = love.graphics.getBlendMode()
    local _love_color = {love.graphics.getColor()}

    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.setBlendMode("alpha", "alphamultiply")

    -- Creates the texture
    local _shadowTexture = love.graphics.newCanvas(width + 24, height + 24)
    _shadowTexture:renderTo(function ()
        love.graphics.setCanvas(_shadowTexture)
        love.graphics.clear()
        onDraw(width + 24, height + 24)
    end)

    -- Creates the buffer
    local _buffer = love.graphics.newCanvas(width + 24, height + 24)

    -- Set the shader
    love.graphics.setShader(_material_shadow_shader)

    -- Draws the texture with horizontal blur
    _buffer:renderTo(function ()
        love.graphics.clear()
        _material_shadow_shader:send('direction', {1 / (width + 24), 0})
        love.graphics.draw(_shadowTexture, 0, 0)
    end)

    -- Draws the texture with verticar blur
    _shadowTexture:renderTo(function ()
        love.graphics.clear()
        _material_shadow_shader:send('direction', {0, 1 / (height + 24)})
        love.graphics.draw(_buffer, 0, 0)
    end)

    love.graphics.setShader(_love_shader)
    love.graphics.setCanvas(_love_canvas)
    love.graphics.setBlendMode(_love_blendmode, _love_alphamode)
    love.graphics.setColor(_love_color)

    _buffer:release()
    return _shadowTexture
end

return boxblur
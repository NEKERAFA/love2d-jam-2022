local gradient = {
    sprite = love.graphics.newCanvas(128, 128),
    shader = love.graphics.newShader("assets/shaders/gradient.glsl")
}

function gradient:draw(colour)
    self.sprite:renderTo(function ()
        love.graphics.clear(colour)
    end)

    local width, height = love.graphics.getDimensions()

    local shader = love.graphics.getShader()
    love.graphics.setShader(self.shader)
    love.graphics.draw(self.sprite, 0, 0, 0, width / 128, height / 128)
    love.graphics.setShader(shader)
end

return gradient
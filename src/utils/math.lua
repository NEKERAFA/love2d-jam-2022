local mathUtils = {}

function mathUtils.round(n)
    return math.floor(n + 0.5)
end

function mathUtils.includeRect(x, y, width, height, px, py)
    return (px >= x) and
        (px <= x + width) and
        (py >= y) and
        (py <= y + height)
end

return mathUtils
local tableUtils = {}

function tableUtils.any(tbl)
    return next(tbl) ~= nil
end

return tableUtils